import { Component } from 'react'
import { NavLink } from 'react-router-dom'

class NavBar extends Component{
   render(){
       return(
           <div>
            <hr />
                <div className="" align="center">
                    <NavLink to='/home'>Angular</NavLink> |&nbsp;
                    <NavLink to='/bootstrap'>Bootstrap&nbsp;</NavLink>|&nbsp;
                    <NavLink to="/git">Git</NavLink>&nbsp;|&nbsp;
                    <NavLink to="/java">Java</NavLink>&nbsp;|&nbsp;
                    <NavLink to="/java">Javascript</NavLink>&nbsp;|&nbsp;
                    <NavLink to="/jquery">Jquery</NavLink>&nbsp;|&nbsp;
                    <NavLink to="/mysql">Mysql</NavLink>&nbsp;|&nbsp;
                    <NavLink to="/mysql">Node</NavLink>&nbsp;|&nbsp;
                    <NavLink to="/mysql">Php</NavLink>&nbsp;|&nbsp;
                    <NavLink to="/git">React</NavLink>
                </div>
                <hr />                   
            </div>
       )
   }        
    
}

export default NavBar
//import logo from './logo.svg';
import './App.css';
import NavBar from './NavBar';
import {BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from '../Home';
import MemoBootstrap from './MemoBootstrap';

function App() {
  return (
    <section id="main">      
      <BrowserRouter>
        <NavBar />        
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/home' element={<Home />} />
          <Route path='/bootstrap' element={<MemoBootstrap />} />
        </Routes>        
      </BrowserRouter>
    </section>
  );
}

export default App;

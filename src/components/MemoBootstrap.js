import { Component } from "react";
import './Main.css'

class MemoBootstrap extends Component{
    render(){
        return(
            <div>
                <h3 align="center">Bootstrap</h3>
                <div className="content">
                    Content Bootstrap
                </div>
            </div>
        )
    }
    
}

export default MemoBootstrap
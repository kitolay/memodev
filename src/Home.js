import { Component } from "react"
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import './components/Main.css'
import { Container, Row, Col } from 'react-bootstrap'
import { CNavItem, CSidebar, CSidebarBrand, CSidebarNav } from '@coreui/react'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import { findDOMNode } from "react-dom"

//const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop) 
/*
scrollToDiv = id => () =>{  
    const divElement = document.getElementById(id)  
    var topPos = divElement.offsetTop

    alert("Top Position = "+topPos)

    topPos = -topPos
    
    /*divElement.scrollIntoView({ behavior: 'smooth', top: topPos, alignToTop: true})
    
    window.scrollTo(100, 5)
        
  }  
*/
/*
async function get_summary(){
    const apiURL = 'http://localhost:8282/summary'
    let json_data = 0

    await fetch(apiURL)
        .then(res => res.json())
        .then(data => json_data = data.length)
        .catch(error => {alert("Erreur api")})          
    
    alert('J L = '+json_data)
    return json_data
}
*/


class Home extends Component{
    constructor(props){
        console.log("constructor")
        
        super(props)  
   
        this.state = {
            json_data: []
        }
    }

    componentDidMount(){          
        const apiURL = 'http://localhost:8282/summary/'
        fetch(apiURL)
            .then(response => response.json())
            .then(data => this.setState({ json_data: data }))
            .catch(error => {alert('Erreur API')})
    }     


    render(){
        const { json_data } = this.state
      
        return (
            <div>
                <h3 align="center">{/*<FontAwesomeIcon icon={faHome} />*/}Angular</h3>
                <div className="content">  
                    <div className="container-fluid">
                        <Row>
                            <Col lg="2" style={{borderRight: '5px solid red'}}>
                               <CSidebar>
                                   <CSidebarBrand>Sommaire</CSidebarBrand>
                                   <CSidebarNav>
                                       <CNavItem href="/#introduction">Introduction</CNavItem>
                                       <CNavItem href="/#commands_utils">Commandes utiles</CNavItem>
                                       <CNavItem href="/#node_npm">Node et npm</CNavItem>                                     
                                       <CNavItem href="/#cli">Angular CLI</CNavItem>  
                                       {json_data.map(key => (
                                            <CNavItem key={key.id} href="/#id">{key.title}</CNavItem>
                                       ))
                                       }                                                                   
                                   </CSidebarNav>
                               </CSidebar>                            
                            </Col>
                            <Col lg="10">
                                <div style={{overflowY: 'scroll', height: '500px'}}>                                   
                                    <div className="col_text">
                                        <h4 id="introduction">Introduction</h4>
                                        <span>
                                        Angular est un framework Javascript côté client qui permet de réaliser des applications Cross-platform Web, Mobilie et Desktop.
                                        Angular est Open Source et développé par Google. Il utilise l’architecture MVM (Modèle Vue Modèle), proche du modèle MVC.
                                        Cela va permettre de structurer son code et bien séparer la vue (l’interface) des modèles (fonctionnement) avec ce que l’on appelle les composants .
                                        Les composants constituent le bloc de construction le plus élémentaire d’une interface utilisateur dans une application Angular, ce qui implique donc qu’une application Angular est une arborescence de composants.
                                        <br />
                                        Le code source d’Angular est écrit en TypeScript. Le TypeScript est une couche supérieur au Javascript développé par Microsoft qui se compile en JavaScript simple. Etant un language typé, il permet de créer des classes, des variables, des signatures de fonction et l’utilisation de modules. Il est important de noter que l’utilisation du TypeScript est facultative, on peut tout à fait utiliser du JavaScript dans un fichier TypeScript.
                                        </span>
                                    </div>
                                <div>
                                    <h4 id="commands_utils">Commandes utiles</h4>  
                                    <ul>
                                        <li><b> npm install -g create-react-app</b> : installation package pour créer un projet</li>   
                                        <li><b> npx create-react-app (name) :</b> créer un projet</li>
                                        <li><b> npm cache clean -f / npm install -g n stable :</b> update node</li>
                                        <li><b> yarn install :</b> installation d'un package</li>
                                        <li><b> yarn init -y :</b> initialisation rapide du projet</li>
                                        <li><b> yarn add react :</b> installation yarn</li>
                                        <li><b> yarn react remove react :</b> désintaller react</li>
                                        <li><b> npm install -g create-react-native-app :</b> installation du package create-react-native-app</li>
                                        <li><b> create-react-native-app first-project :</b> créer un nouveau projet</li>
                                        <li><b> npm install -g expo-cli or yarn global add expo-cli :</b> installation expo cli</li> 
                                    </ul>                                
                                </div>
                                <div className="col_text">
                                    <h4 id="node_npm">Node et npm</h4>
                                    <span>
                                        Node.js est une plateforme de développement JavaScript.<br />
                                        Les best practices sont les meilleures pratiques préconisées par l'équipe d'angular.<br />
                                        Node.js est la plateforme pour développer notre application.<br />
                                        Npm est le gestionnaire des librairies (packages en anglais)<br />                                
                                    </span>
                                </div>                               
                                <div className="col_text">
                                    <h4 id="cli">Angular CLI</h4>
                                    <span>
                                    Angular CLI est un outil pour initialiser, développer et maintenir des applications Angular. 
                                    Angular CLI pour la mise en place de l'architecture du projet. C’est une librairie ou package.
                                    Angular Command Line Interface (CLI).
                               
                                    </span>
                                </div>  
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>                
                <div align="center">
                    <button>Get me</button>
                </div>
            </div>
        );
    }
}

export default Home